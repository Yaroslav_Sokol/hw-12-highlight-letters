// знаходимо всі кнопки на сторінці
const buttons = document.querySelectorAll('.btn');

// створюємо обробник події "keydown" на документі
document.addEventListener('keydown', function(event) {
  // отримуємо код натиснутої клавіші
  const keyCode = event.keyCode;

  // знаходимо кнопку за її текстовим вмістом
  const button = Array.from(buttons).find(button => button.textContent === String.fromCharCode(keyCode));

  if (button) {
    // знімаємо підсвічення з усіх кнопок
    buttons.forEach(button => button.classList.remove('active'));

    // додаємо підсвічення на натиснуту кнопку
    button.classList.add('active');
  }
});
